@file:OptIn(ExperimentalTvMaterial3Api::class)

package com.open.launcher.screen

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.tv.foundation.lazy.grid.TvGridCells
import androidx.tv.foundation.lazy.grid.TvLazyVerticalGrid
import androidx.tv.foundation.lazy.grid.items
import androidx.tv.material3.ExperimentalTvMaterial3Api
import androidx.tv.material3.StandardCardLayout
import androidx.tv.material3.Text
import com.open.launcher.bean.getGridDatas

@Composable
fun AppScreen() {
    TvLazyVerticalGrid(
        columns = TvGridCells.Fixed(6),
        verticalArrangement = Arrangement.spacedBy(16.dp),
        horizontalArrangement = Arrangement.spacedBy(16.dp),
        contentPadding = PaddingValues(90.dp)
    ) {
        items(getGridDatas()) {
            Text(text = "${it.name}", color= Color.White)
            StandardCardLayout(
                imageCard = {},
                title = { }
            )
        }
    }
}
