@file:OptIn(ExperimentalTvMaterial3Api::class)

package com.open.launcher.screen

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.tv.foundation.lazy.list.TvLazyColumn
import androidx.tv.foundation.lazy.list.TvLazyRow
import androidx.tv.material3.ExperimentalTvMaterial3Api
import androidx.tv.material3.Icon
import androidx.tv.material3.ListItem
import androidx.tv.material3.ListItemDefaults
import androidx.tv.material3.MaterialTheme
import androidx.tv.material3.Text
import com.open.launcher.R

@Composable
fun MyScreen() {

    TvLazyColumn(
        modifier = Modifier.fillMaxSize()
    ) {
        item {
            TvLazyRow() {
                item {
                }
            }
        }
        item {

        }
    }

//    ListItem(
//        headlineContent = {
//            Text(
//                text = "测试",
//                style = MaterialTheme.typography.bodyMedium.copy(
//                    fontWeight = FontWeight.Medium
//                ),
//                modifier = Modifier.fillMaxWidth()
//            )
//        },
//        trailingContent = {
//            Icon(
//                modifier = Modifier
//                    .padding(vertical = 2.dp)
//                    .padding(start = 4.dp)
//                    .size(20.dp),
//                painter = painterResource(id = R.drawable.ic_launcher_background),
//                contentDescription = ""
//                )
//        },
//        scale = ListItemDefaults.scale(focusedScale = 1f),
//        selected = false,
//        onClick = {},
//        shape = ListItemDefaults.shape(shape = MaterialTheme.shapes.extraSmall),
//        colors = ListItemDefaults.colors(
//            focusedContainerColor = MaterialTheme.colorScheme.inverseSurface,
//            selectedContainerColor = MaterialTheme.colorScheme.inverseSurface
//                .copy(alpha = 0.4f),
//            selectedContentColor = MaterialTheme.colorScheme.surface,
//        )
//    )
}