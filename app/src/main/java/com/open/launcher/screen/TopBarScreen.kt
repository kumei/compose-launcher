@file:OptIn(ExperimentalTvMaterial3Api::class, ExperimentalComposeUiApi::class,
    ExperimentalTvMaterial3Api::class
)

package com.open.launcher.screen

import androidx.compose.animation.core.animateDpAsState
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.key
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.focus.focusRestorer
import androidx.compose.ui.focus.onFocusChanged
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.DpRect
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.height
import androidx.compose.ui.unit.sp
import androidx.compose.ui.unit.width
import androidx.compose.ui.zIndex
import androidx.tv.material3.ExperimentalTvMaterial3Api
import androidx.tv.material3.MaterialTheme
import androidx.tv.material3.ShapeDefaults
import androidx.tv.material3.Tab
import androidx.tv.material3.TabDefaults
import androidx.tv.material3.TabRow
import androidx.tv.material3.Text


val TopBarTabs = listOf("我的", "影视", "应用")
// +1 for ProfileTab
val TopBarFocusRequesters = List(size = TopBarTabs.size) { FocusRequester() }

/**
 * 标题栏(我的，电影，电视剧 等)
 */
@Composable
fun TopBarScreen(
    modifier: Modifier = Modifier,
    selectedTabIndex: Int,
    onScreenSelection: (index:Int, title: String) -> Unit = {index,title->}
) {
    var isTabRowFocused by remember { mutableStateOf(false) }
    TabRow(
        modifier = modifier
            .padding(top = 40.dp, start = 90.dp)
            .focusRestorer()
            .onFocusChanged {
                isTabRowFocused = it.isFocused || it.hasFocus
            },
        selectedTabIndex = selectedTabIndex,
        indicator = { tabPositions, isActivated ->
            if (selectedTabIndex >= 0) {
                TopBarMoveIndicator(
                    currentTabPosition = tabPositions[selectedTabIndex],
                    isFocused = isTabRowFocused
                )
            }
        },
        separator = { Spacer(modifier = Modifier) },
    ) {
        TopBarTabs.forEachIndexed { index, title ->
            key(index) {
                Tab(
                    colors = TabDefaults.pillIndicatorTabColors(
                        contentColor = Color.White,
                        focusedContentColor = Color.Black,
                        selectedContentColor = Color.White,
                    ),
                    modifier = Modifier
                        .height(60.dp)
                        .focusRequester(TopBarFocusRequesters[index]),
                    selected = index == selectedTabIndex,
                    onFocus = { onScreenSelection(index, title) },
                    onClick = { }
                ) {
                    val weight = if (index == selectedTabIndex) FontWeight.Bold else FontWeight.Normal
                    Text(
                        modifier = Modifier
                            .fillMaxSize()
                            .wrapContentSize()
                            .padding(horizontal = 30.dp),
                        textAlign = TextAlign.Center,
                        text = title,
                        style = MaterialTheme.typography.titleLarge.copy(fontSize = 32.sp, fontWeight = weight)
                    )
                }
            }
        }
    }
}

/**
 * 标题栏移动的白色色块
 */
@OptIn(ExperimentalTvMaterial3Api::class)
@Composable
fun TopBarMoveIndicator(
    currentTabPosition: DpRect,
    isFocused: Boolean
) {
    val width by animateDpAsState(targetValue = currentTabPosition.width, label = "width")
    val height = if (isFocused) currentTabPosition.height else 2.dp
    val leftOffset by animateDpAsState(targetValue = currentTabPosition.left, label = "leftOffset")
    val topOffset = currentTabPosition.top

//    val moveColor by animateColorAsState(
//        targetValue = if (isFocused) Color.White else Color.White.copy(alpha = 0.4f),
//        label = "moveColor"
//    )
    val moveColor = Color.White
    val moveShape = if (isFocused) ShapeDefaults.ExtraLarge else ShapeDefaults.ExtraSmall

    Box(
        modifier = Modifier
            .fillMaxWidth()
            .wrapContentSize(Alignment.BottomStart)
            .offset(leftOffset, topOffset)
            .width(width)
            .height(height)
            .background(color = moveColor, shape = moveShape)
            .zIndex(-1f)
    )
}