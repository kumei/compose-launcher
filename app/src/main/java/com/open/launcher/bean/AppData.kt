package com.open.launcher.bean

data class AppData(
    val name:String,
    val icon:String
)
