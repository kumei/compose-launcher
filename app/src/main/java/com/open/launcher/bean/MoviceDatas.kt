package com.open.launcher.bean

data class MoviceData(
    val url:String,
    val title:String,
    var bgUrl:String="",
    val detail:String="",
)

fun getImmersDatas():List<MoviceData> {
    return listOf(
        MoviceData("https://1b2a.net/img/mv/BGMm.webp", "蜘蛛侠：纵横宇宙", "https://1b2a.net/img/mv/i_BGMm.webp",
            "影片讲述了新生代蜘蛛侠迈尔斯（沙梅克·摩尔 Shameik Moore 配音）携手蜘蛛格温（海莉·斯坦菲尔德 Hailee Steinfeld 配音），穿越多元宇宙踏上更宏大的冒险征程的故事。"
        ),
        MoviceData("https://i0.hdslb.com/bfs/bangumi/image/196c7ca9e29c6cdb30be065f27aa518cbda0c75d.jpg@2560w_500h.webp",
            "速度与激情10", "", "当家人们陷入危机，唐老大（范·迪塞尔 饰）为救飞车家族再度出山。终途启程，这场前所未有的疾速狂飙，你准备好了吗？"),
        MoviceData("https://1b2a.net/img/mv/D067.webp", "达荷美女战士",
            "https://i0.hdslb.com/bfs/bangumi/image/215e34c511aefbdecdaeddb28b3dbeddfece824a.png@2560w_500h.webp", "影片讲述了史上最英勇女战士的非凡故事，改编自发生在19世纪非洲西海岸的真实历史事件。"),
        MoviceData("https://1b2a.net/img/mv/12gM.webp", "惊天营救2 Extraction 2", "https://1b2a.net/img/mv/i_12gM.webp",
            "在 Netflix 动作大片《惊天营救》的续集《惊天营救 2》中，克里斯·海姆斯沃斯将继续饰演泰勒·雷克。在第一部电影的惊险情节中侥幸逃生后，雷克又卷土重来。"
        ),
        MoviceData("https://1b2a.net/img/mv/W7bJ.webp", "超级马力欧兄弟大电影", "https://1b2a.net/img/mv/i_W7bJ.webp",
            "马力欧（克里斯·帕拉特 Chris Pratt 配音）和路易吉（查理·戴 Charlie Day 饰）是生活在布鲁克林的两名水管工，他们刚刚成立了自己的小公司，但很显然，他们的家人并不完全支持兄弟两人的这份事业。"
        ),
        MoviceData("https://1b2a.net/img/mv/i_djEn.webp", "银河护卫队3", "https://1b2a.net/img/mv/i_djEn.webp",
            "影片承接前作，银河护卫队成员们已经在虚无知地上安顿了下来。然而，由于火箭浣熊（布莱德利·库珀 Bradley Cooper 配音）的动荡往事的侵扰，他们的生活很快被打破。"
        ),
        MoviceData("https://1b2a.net/img/mv/ZO6B.webp", "铃芽之旅 すずめの戸締まり", "https://1b2a.net/img/mv/i_ZO6B.webp",
            "宁静的九州乡间小镇，生活着平凡的少女岩户铃芽（原菜乃华 配音）。这天上学路上，她邂逅了神秘的白衣青年宗像草太（松村北斗 配音）。"
        ),
        MoviceData("https://1b2a.net/img/mv/21yz.webp", "疾速追杀4", "https://1b2a.net/img/mv/i_21yz.webp",
            "承接上集于纽约大陆酒店中枪坠楼，大难不死的约翰·威克（基努·里维斯饰）越洋前往大阪大陆酒店，向经理兼老朋友岛津浩二（真田广之饰）求助。"
        )
    )
}

fun getDatas():List<MoviceData> {
    return listOf(
        MoviceData("https://1b2a.net/img/mv/DXw8.webp", "八角笼中"),
        MoviceData("https://1b2a.net/img/mv/DO9G.webp", "犯罪都市3 범죄도시3"),
        MoviceData("https://1b2a.net/img/mv/xJ85.webp", "兜风 Joy Ride "),
        MoviceData("https://1b2a.net/img/mv/jJ8x.webp", "消失的她"),
        MoviceData("https://img1.doubanio.com/view/photo/s_ratio_poster/public/p2897210097.webp", "奥本海默 Oppenheimer"),
        MoviceData("https://1b2a.net/img/mv/xJ7w.webp", "Trap Jazz"),
        MoviceData("https://img9.doubanio.com/view/photo/s_ratio_poster/public/p2895679456.webp", "芭比"),
    )
}

fun getDatasHead():List<MoviceData> {
    return listOf(
        MoviceData("https://img2.doubanio.com/view/photo/s_ratio_poster/public/p2896551721.webp", ""),
        MoviceData("https://1b2a.net/img/mv/dv5V.webp", ""),
        MoviceData("https://1b2a.net/img/mv/8n07.webp", ""),
        MoviceData("https://1b2a.net/img/mv/8a1R.webp", ""),
    )
}

fun getGridDatas():List<AppData> {
    return listOf(
        AppData("芒果TV", ""),
        AppData("云视听小电视", ""),
        AppData("银河奇异果", ""),
        AppData("咪视界", ""),
        AppData("CIBN酷喵", ""),
        AppData("QQ音乐", ""),
        AppData("应用市场", ""),
        AppData("网易云音乐", ""),
        AppData("系统设置", ""),
        AppData("资源管理器", ""),
        AppData("系统升级", ""),
    )
}

fun getDatas1():List<MoviceData> {
    return listOf(
        MoviceData("https://img2.doubanio.com/view/photo/s_ratio_poster/public/p2896551721.webp", "孤注一掷"),
        MoviceData("https://1b2a.net/img/mv/dv5V.webp", "超能一家人"),
        MoviceData("https://1b2a.net/img/mv/8n07.webp", "来自另一个星球的女孩"),
        MoviceData("https://1b2a.net/img/mv/8a1R.webp", "灌篮高手"),
        MoviceData("https://1b2a.net/img/mv/eJdK.webp", "扫毒3：人在天涯"),
        MoviceData("https://1b2a.net/img/mv/eJdK.webp", "扫毒3：人在天涯"),
        MoviceData("https://1b2a.net/img/mv/yJKb.webp", "芭比"),
    )
}

fun getDatas2():List<MoviceData> {
    return listOf(
        MoviceData("https://1b2a.net/img/mv/7V2o.webp", "疯狂元素城"),
        MoviceData("https://1b2a.net/img/mv/7VLY.webp", "不能错过的只有你"),
        MoviceData("https://1b2a.net/img/mv/Jxv6.webp", "哆啦A梦：大雄与天空的理想乡"),
        MoviceData("https://1b2a.net/img/mv/DOy7.webp", "我爱你"),
        MoviceData("https://1b2a.net/img/mv/52Ae.webp", "严密的关系"),
        MoviceData("https://1b2a.net/img/mv/52Ae.webp", "严密的关系"),
        MoviceData("https://1b2a.net/img/mv/52Ae.webp", "严密的关系"),
    )
}

fun getData3():List<MoviceData> {
    return listOf(
        MoviceData("https://1b2a.net/img/mv/xJWM.webp", "怪物少女妮莫娜 Nimona"),
        MoviceData("https://1b2a.net/img/mv/Yyr1.webp", "The List"),
        MoviceData("https://1b2a.net/img/mv/W77O.webp", "闪电侠 The Flash"),
        MoviceData("https://1b2a.net/img/mv/k3J1.webp", "Irati"),
        MoviceData("https://1b2a.net/img/mv/R0YA.webp", "变形金刚：超能勇士崛起"),
        MoviceData("https://1b2a.net/img/mv/R0YA.webp", "变形金刚：超能勇士崛起"),
        MoviceData("https://1b2a.net/img/mv/R0YA.webp", "变形金刚：超能勇士崛起"),
    )
}
