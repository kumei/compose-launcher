pluginManagement {
    repositories {
        // 阿里云镜像 https://maven.aliyun.com/mvn/guide
        maven(url = "http://maven.aliyun.com/repository/google") {
            isAllowInsecureProtocol = true
        }
        maven(url = "http://maven.aliyun.com/repository/public") {
            isAllowInsecureProtocol = true
        }
        maven(url = "http://maven.aliyun.com/repository/gradle-plugin") {
            isAllowInsecureProtocol = true
        }
        // 阿里云云效仓库：https://maven.aliyun.com/mvn/guide
        maven(url = "http://maven.aliyun.com/repository/jcenter") {
            isAllowInsecureProtocol = true
        }
        // 华为开源镜像：https://mirrors.huaweicloud.com
        maven(url = "http://repo.huaweicloud.com/repository/maven") {
            isAllowInsecureProtocol = true
        }
        // JitPack 远程仓库：https://jitpack.io
        maven(url = "https://jitpack.io")
        // MavenCentral 远程仓库：https://mvnrepository.com
        mavenCentral()
        google()
        gradlePluginPortal()
    }
}
dependencyResolutionManagement {
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
    repositories {
        // 阿里云镜像 https://maven.aliyun.com/mvn/guide
        maven(url = "http://maven.aliyun.com/repository/google") {
            isAllowInsecureProtocol = true
        }
        maven(url = "http://maven.aliyun.com/repository/public") {
            isAllowInsecureProtocol = true
        }
        maven(url = "http://maven.aliyun.com/repository/gradle-plugin") {
            isAllowInsecureProtocol = true
        }
        // 阿里云云效仓库：https://maven.aliyun.com/mvn/guide
        maven(url = "http://maven.aliyun.com/repository/jcenter") {
            isAllowInsecureProtocol = true
        }
        // 华为开源镜像：https://mirrors.huaweicloud.com
        maven(url = "http://repo.huaweicloud.com/repository/maven") {
            isAllowInsecureProtocol = true
        }
        // JitPack 远程仓库：https://jitpack.io
        maven(url = "https://jitpack.io")
        // MavenCentral 远程仓库：https://mvnrepository.com
        mavenCentral()
        google()
    }
}

rootProject.name = "ComposeDesktop"
include(":app")
